import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { StatusCodes } from 'http-status-codes';
import { test } from '../api/testConnection';

export async function handler(event?: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
    try {
        const user = await test();

        return {
            statusCode: StatusCodes.OK,
            body: JSON.stringify(user),
        };
    } catch (error: any) {
        return {
            statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
            body: JSON.stringify(error.message),
        };
    }
}
