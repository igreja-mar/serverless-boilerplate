import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { StatusCodes } from 'http-status-codes';

import { check } from '../api/test';

export async function handler(event?: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
    const checked = await check();

    return {
        statusCode: StatusCodes.OK,
        body: JSON.stringify(checked),
    };
}
