import { ConnectionOptions } from 'typeorm';

export const configs: ConnectionOptions = {
    type: 'mongodb',

    database: 'test',
    url: process.env.MONGO_URI,

    useNewUrlParser: true,
    useUnifiedTopology: true,

    synchronize: true,
    logging: true,

    entities: ['dist/core/models/**/*.{js,ts}'],
    migrations: ['dist/database/migration/**/*.{js,ts}'],
    subscribers: ['dist/subscriber/**/*.{js,ts}'],

    cli: {
        entitiesDir: 'dist/core/models',
        migrationsDir: 'dist/database/migration',
        subscribersDir: 'dist/database/subscriber',
    },
};
