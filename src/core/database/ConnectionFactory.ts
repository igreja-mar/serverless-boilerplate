import 'reflect-metadata';
import { Connection, createConnection, Entity } from 'typeorm';

import { configs } from './config';

class ConnectionFactory {
    private connection?: Connection = undefined;

    async open(): Promise<void> {
        this.connection = await createConnection(configs);
    }

    async close(): Promise<void> {
        return this.connection?.close();
    }

    async save(entity: object): Promise<any> {
        try {
            return await this.connection?.manager.save(entity);
        } catch (error) {
            throw error;
        }
    }

    async find({ entity, filter }: { entity: any; filter?: any }): Promise<any> {
        try {
            return await this.connection?.manager.find(entity, filter);
        } catch (error) {
            throw error;
        }
    }

    async delete({ entity, filter }: { entity: any; filter?: any }): Promise<any> {
        try {
            return await this.connection?.manager.delete(entity, filter);
        } catch (error) {
            throw error;
        }
    }
}

export const connection: ConnectionFactory = new ConnectionFactory();
