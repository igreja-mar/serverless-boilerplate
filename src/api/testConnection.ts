import { connection } from '../core/database/ConnectionFactory';
import { User } from '../core/models/User';

export async function test(): Promise<any> {
    try {
        await connection.open();

        const user = new User();
        user.firstName = 'serverless';
        user.lastName = 'boilerplate';
        user.age = 21;

        await connection.save(user);

        const newUser = await connection.find({
            entity: User,
            filter: {
                _id: user.id,
            },
        });
        console.log(newUser);

        const result = await connection.delete({
            entity: User,
            filter: user.id,
        });
        console.log(result);

        await connection.close();

        return true;
    } catch (error) {
        throw error;
    }
}
