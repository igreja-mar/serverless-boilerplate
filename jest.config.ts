import type { Config } from '@jest/types';

export default async (): Promise<Config.InitialOptions> => {
    return {
        verbose: true,
        preset: 'ts-jest',
        testEnvironment: 'node',

        coverageDirectory: 'coverage',
        collectCoverageFrom: ['<rootDir>/src/**/*.ts'],
        collectCoverage: true,
        coverageReporters: ['json', 'html'],

        transform: {
            '.+\\.ts$': 'ts-jest',
        },

        testMatch: ['<rootDir>/__tests__/**/*.(test|spec).ts'],
    };
};
