import { check } from '../../src/api/test';

describe('teste api promise', () => {
    it('resolve check', async () => {
        const checked = await check();
        expect(checked).toStrictEqual({
            status: 'running',
            version: 'infdev',
        });
    });
});
