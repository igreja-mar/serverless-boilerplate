import { APIGatewayProxyEvent } from 'aws-lambda';
import { handler } from '../../src/lambda/test';

const event: jest.Mocked<APIGatewayProxyEvent> = {} as any;

describe('lambda de teste', () => {
    it('deve retornar status 200', async () => {
        const result = await handler(event);

        expect(result.statusCode).toBe(200);
    });
    it('deve retornar no `body` o check com version e status', async () => {
        const result = await handler(event);

        expect(JSON.parse(result.body)).toStrictEqual({
            status: 'running',
            version: 'infdev',
        });
    });
});
